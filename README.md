# Thème Kit de démarrage pour spipr v2

Thème dépouillé pour SPIPr v2 servant de point de départ pour créer son propre thème. Plutôt destiné aux personnes ayant des connaissances en CSS et SASS/SCSS.

## Histoire
Devant développer un thème avec SPIPr et Bootstrap, j'ai fait face à de nombreuses erreurs de compilation SCSS. Mes recherches n'ont pas apporté de solution, j'ai donc décidé de repartir de zéro pour résoudre le problème. Après de nombreux essais/erreurs, j'ai trouvé une organisation de fichiers qui fonctionne et permet de modifier simplement les variables bootstrap (plus besoin d'importer la totalité des variables et d'ajouter `!default` à la fin de chaque ligne).

## Installation
Il faut copier/coller ou cloner ce dépôt dans le dossier squelettes/themes/ de SPIP.

## Fonctionnement
:memo: *Vous trouverez de nombreux commentaires dans les fichiers pour vous aiguiller.* :memo:

Les deux fichiers importants sont `_variables.scss` et `themes.scss`. Les autres fichiers sont présents à titre d'exemple, et sont supprimables/renommables.

### \_variables.scss
On y modifie les variables de bootstrap.  
La [liste des variables de bootstrap](https://github.com/twbs/bootstrap/blob/v4.6.1/scss/_variables.scss) et la [documentation Bootstrap 4.6](https://getbootstrap.com/docs/4.6/getting-started/theming/#sass) vous seront bien utiles.

### themes.scss
On y importe dans l'ordre :

1. bootstrap
2. les fichiers contenant nos propres variables et mixins
3. nos fichiers de style

En faisant cela, on rend disponible dans **tous les fichiers importés en 3.** les variables bootstrap et nos variables. Donc nous n'avons pas besoin de les réimporter à chaque fois que l'on crée un fichier.

### vos fichiers
Créez un nouveau fichier en le préfixant d'un underscore (ex: `_monfichier.scss`)  
Ouvrez `theme.scss` et ajouter une ligne `@import "monfichier";` au bon endroit (*en 2.* si ce sont des variables, *en 3.* si ce sont des styles).

:boom: :warning: :boom: Si votre fichier porte le même nom qu'un des fichiers scss de bootstrap, il sera automatiquement importé par SPIPr/Bootstrap4, ce qui créera sans doute des conflits. [Voir la liste des fichiers de bootstrap](https://github.com/twbs/bootstrap/tree/v4.6.1/scss) :boom: :warning: :boom:

Et bien sûr n'oubliez pas de remplacer les informations dans `paquet.xml` et de créer une vignette.

## Autres liens utiles
- [documentation SPIPr pour créer son propre thème](https://spipr.nursit.com/themes)
- [documentation SASS](https://sass-lang.com/documentation/)